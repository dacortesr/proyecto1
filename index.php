<?php
session_start();
require_once 'logica/Administrador.php';
require_once 'logica/Cliente.php';
require_once 'logica/Provedor.php';
require_once 'logica/Categoria.php';
require_once 'logica/Libro.php';
require_once 'logica/Carro.php';
require_once 'logica/Compra.php';
require_once 'logica/Reporte.php';
require_once 'logica/Repartidor.php';
$pid = "";
if (isset($_GET["pid"])) {
	$pid = base64_decode($_GET["pid"]);
}

if (isset($_GET["sesion"]) && $_GET["sesion"] == "false") {
	$_SESSION["id"] = "";
	$_SESSION["rol"] = "";
}
$paginasSinSesion = array(
	"presentacion/encabezado.php",
	"presentacion/autenticar.php",
	"presentacion/crear_cuenta.php"
)

?>
<!Doctype html>
<html>

<head>
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.1/css/all.css" />
	<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
</head>
<?php
if ($pid != "") {
	if (in_array($pid, $paginasSinSesion)) {
		include $pid;
	} else {
		if (isset($_SESSION["id"]) && $_SESSION["rol"] != "") {
			include $pid;
		} else {
			include 'presentacion/ingresar.php';
		}
	}
} else {
	include 'presentacion/encabezado.php';
}

?>

</html>