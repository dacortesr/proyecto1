<?php
session_start();
require_once 'logica/Cliente.php';
require_once 'logica/Provedor.php';
require_once 'logica/Categoria.php';
require_once 'logica/Libro.php';
require_once 'logica/Carro.php';
require_once 'logica/Compra.php';
require_once 'logica/Reporte.php';
require_once 'logica/Domicilio.php';
require_once 'fpdf/fpdf.php';

$Domicilio = new Domicilio();
$Dom = $Domicilio ->consultar3();
$Do = $Domicilio->consultar4();
$pdf = new FPDF('P','mm', 'Letter');
$pdf -> SetMargins(50, 10, 10);
$pdf -> AddPage();
$pdf -> Image("imagenes/hi.png", 10, 10, 20, 20);
$pdf -> SetFont('Times', 'B', 18);
//$pdf -> Text(20, 20, 'Hola Mundo');
$pdf -> Cell(120, 20, "Reporte", 0, 1, 'C');

$pdf -> SetFont('Times', 'B', 10);

$pdf -> Cell(10, 8, "", 0, 0, 'C');
$pdf -> Cell(80, 8, "Entregados", 1, 1, 'C');

$pdf -> Cell(10, 8, "#", 1, 0, 'C');
$pdf -> Cell(50, 8, "Identificacion ", 1, 0, 'C');
$pdf -> Cell(30, 8, "Direccion", 1, 1, 'C');

$pdf -> SetFont('Times', '', 10);
$i = 1;
foreach($Dom as $r){
$pdf->Cell(10, 8, $i++, 1, 0, 'C');
$pdf->Cell(50, 8, $r->getIdcliente(), 1, 0, 'C');
$pdf->Cell(30, 8, $r->getDireccion(), 1, 1, 'C');
}
$pdf->Ln();
$pdf->Cell(120, 20, "Reporte", 0, 1, 'C');

$pdf->SetFont('Times', 'B', 10);

$pdf->Cell(10, 8, "", 0, 0, 'C');
$pdf->Cell(80, 8, "Por Entregar", 1, 1, 'C');

$pdf->Cell(10, 8, "#", 1, 0, 'C');
$pdf->Cell(50, 8, "Identificacion ", 1, 0, 'C');
$pdf->Cell(30, 8, "Direccion", 1, 1, 'C');

$pdf->SetFont('Times', '', 10);
$i = 1;
foreach ($Do as $r) {
    $pdf->Cell(10, 8, $i++, 1, 0, 'C');
    $pdf->Cell(50, 8, $r->getIdcliente(), 1, 0, 'C');
    $pdf->Cell(30, 8, $r->getDireccion(), 1, 1, 'C');
}
$pdf -> Output('I');
