<?php
class RepartidorDAO{
    private $id;
    private $nombre;
    private $apellido;
    private $correo;
    private $clave;
    
    public function __construct($id="",$nombre="", $apellido="", $correo="", $clave=""){
        $this -> id = $id;
        $this -> nombre = $nombre;
        $this -> apellido = $apellido;
        $this -> correo = $correo;
        $this -> clave = $clave;
    }

    public function consultar()
    {
        return "select nombre, apellido, correo
                from repartidor
                where idrepartidor = '" . $this->id . "'";
    }

    public function consultarTodos()
    {
        return "select *from repartidor";
    }

    public function autenticar()
    {
        return "select idrepartidor
                from repartidor
                where correo = '" . $this->correo . "' and clave = md5('" . $this->clave . "')";
    }
}
