-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 30-03-2022 a las 08:04:29
-- Versión del servidor: 10.4.16-MariaDB
-- Versión de PHP: 7.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `mindbooks`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `administrador`
--

CREATE TABLE `administrador` (
  `idadministrador` int(11) NOT NULL,
  `nombre` varchar(45) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `apellido` varchar(45) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `correo` varchar(45) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `clave` varchar(45) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `administrador`
--

INSERT INTO `administrador` (`idadministrador`, `nombre`, `apellido`, `correo`, `clave`) VALUES
(1, 'Alejandro', 'Cortes', 'Cortes@correo.com', 'f688ae26e9cfa3ba6235477831d5122e');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `carrito`
--

CREATE TABLE `carrito` (
  `id` int(11) NOT NULL,
  `idcliente` varchar(255) DEFAULT NULL,
  `idprovedor` int(11) NOT NULL,
  `nombrep` varchar(255) DEFAULT NULL,
  `cate` int(11) NOT NULL,
  `precio` decimal(11,0) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoria`
--

CREATE TABLE `categoria` (
  `idcategoria` int(11) NOT NULL,
  `tipo` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `categoria`
--

INSERT INTO `categoria` (`idcategoria`, `tipo`) VALUES
(1, 'Generalidades'),
(2, 'Filosofia y psicologia'),
(3, 'Religion'),
(4, 'Ciencias sociales'),
(5, 'Lenguas'),
(6, 'Ciencias naturales'),
(7, 'Matematicas'),
(8, 'Tecnologia'),
(9, 'Literatura'),
(10, 'Historia');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE `cliente` (
  `idcliente` int(11) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `apellido` varchar(45) NOT NULL,
  `correo` varchar(45) NOT NULL,
  `clave` varchar(45) NOT NULL,
  `fecha_nacimiento` date NOT NULL,
  `solicitud` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `cliente`
--

INSERT INTO `cliente` (`idcliente`, `nombre`, `apellido`, `correo`, `clave`, `fecha_nacimiento`, `solicitud`) VALUES
(2, 'Cristian', 'Perez', 'PerezC@correo.com', '0db7e7a347aaa68a26218df2a01c99c6', '2000-01-30', 2),
(8, 'Jualian', 'Garcia', 'GarciaJ@correo.com', '7013dd8caf235788ddedee06ff8ad0ed', '1998-12-19', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `compras`
--

CREATE TABLE `compras` (
  `id` int(11) NOT NULL,
  `idcliente` int(11) DEFAULT NULL,
  `nombreli` varchar(55) DEFAULT NULL,
  `categoria` int(11) DEFAULT NULL,
  `estadopago` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `compras`
--

INSERT INTO `compras` (`id`, `idcliente`, `nombreli`, `categoria`, `estadopago`) VALUES
(35, 8, 'Dios', 3, 1),
(36, 8, 'Internet', 8, 1),
(37, 8, 'vida', 6, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `domicilio`
--

CREATE TABLE `domicilio` (
  `id` int(11) NOT NULL,
  `idcliente` varchar(150) DEFAULT NULL,
  `direccion` varchar(500) DEFAULT NULL,
  `estado` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `domicilio`
--

INSERT INTO `domicilio` (`id`, `idcliente`, `direccion`, `estado`) VALUES
(5, '8', 'Calle 43 #57-32', 2),
(6, '8', NULL, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `libro`
--

CREATE TABLE `libro` (
  `idlibro` int(11) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `autor` varchar(45) NOT NULL,
  `precio` decimal(11,0) NOT NULL,
  `Proveedor_idProveedor` int(11) NOT NULL,
  `categoria_idcategoria` int(11) NOT NULL,
  `ruta` varchar(10000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `libro`
--

INSERT INTO `libro` (`idlibro`, `nombre`, `autor`, `precio`, `Proveedor_idProveedor`, `categoria_idcategoria`, `ruta`) VALUES
(3, 'Amor y odio', 'jorge', '30000', 100, 1, 'imagenes/libro(2).png'),
(4, 'vida', 'jorge', '25000', 100, 6, 'imagenes/libro-de-ninos.png'),
(5, 'Va mas allá de un muro', 'jorge', '40000', 100, 10, 'imagenes/libro.png'),
(6, 'Operaciones Avanzadas', 'jorge', '15000', 100, 7, 'imagenes/libro (3).png'),
(7, 'Algebra principiantes', 'jorge', '10000', 100, 7, 'imagenes/libro-de-matematicas.png'),
(44, 'Calculo especial', 'Rafael', '12000', 1, 7, 'imagenes/libro-de-matematicas.png'),
(47, 'Internet', 'Rafael', '21000', 1, 8, 'imagenes/lector-electronico.png'),
(48, 'hsks', 'Julio', '120000', 102, 2, 'imagenes/libro-de-tapa-negra-cerrado.png'),
(49, 'Dios', 'Javier', '30000', 104, 3, 'imagenes/biblia.png');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proveedor`
--

CREATE TABLE `proveedor` (
  `idProveedor` int(11) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `apellido` varchar(45) NOT NULL,
  `correo` varchar(45) NOT NULL,
  `clave` varchar(45) NOT NULL,
  `editorial` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `proveedor`
--

INSERT INTO `proveedor` (`idProveedor`, `nombre`, `apellido`, `correo`, `clave`, `editorial`) VALUES
(1, 'Rafael', 'Nuñez', 'Rafa@correo.com', '35cd2d0d62d9bc5e60a3ca9f7593b05b', 'Distrital'),
(100, 'jorge', 'alvaraez', 'JorgeA@correo.com', 'f4a1c8901a3d406f17af67144a3ec71a', 'Viña'),
(101, 'Kil', 'orian', 'Orik@correo.com', '9854472be24754f282191a46bc5f626a', 'Rai'),
(102, 'Julio', 'Perez', 'JulioP@correo.com', '5d2dc4ad113b3b11c54a9f348bd50af0', 'Fia'),
(103, 'jk', 'ty', 'kj@correo.com', '34c734f845a3165186d4c7f3aff0568d', 'Real'),
(104, 'Javier', 'Pastrana', 'Jp@correo.com', '484c05a60b0e997cc78dea3341356feb', 'Altrea');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `repartidor`
--

CREATE TABLE `repartidor` (
  `idrepartidor` int(11) NOT NULL,
  `nombre` varchar(150) DEFAULT NULL,
  `apellido` varchar(500) DEFAULT NULL,
  `correo` varchar(500) DEFAULT NULL,
  `clave` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `repartidor`
--

INSERT INTO `repartidor` (`idrepartidor`, `nombre`, `apellido`, `correo`, `clave`) VALUES
(1, 'Alfonzo', 'Urrea', 'UrreaAlfo@correo.com', '00fe219c97d1af5e765ce5574352be43');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reporte`
--

CREATE TABLE `reporte` (
  `idcliente` int(11) NOT NULL,
  `idprovedor` int(11) NOT NULL,
  `nombreli` varchar(30) NOT NULL,
  `categoria` varchar(30) NOT NULL,
  `valorunitario` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `reporte`
--

INSERT INTO `reporte` (`idcliente`, `idprovedor`, `nombreli`, `categoria`, `valorunitario`) VALUES
(8, 104, 'Dios', '3', '30000'),
(8, 1, 'Internet', '8', '21000'),
(8, 104, 'Dios', '3', '30000'),
(8, 1, 'Internet', '8', '21000'),
(8, 104, 'Dios', '3', '30000'),
(8, 1, 'Internet', '8', '21000'),
(8, 100, 'vida', '6', '25000');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `administrador`
--
ALTER TABLE `administrador`
  ADD PRIMARY KEY (`idadministrador`);

--
-- Indices de la tabla `carrito`
--
ALTER TABLE `carrito`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `categoria`
--
ALTER TABLE `categoria`
  ADD PRIMARY KEY (`idcategoria`);

--
-- Indices de la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`idcliente`);

--
-- Indices de la tabla `compras`
--
ALTER TABLE `compras`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `domicilio`
--
ALTER TABLE `domicilio`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `libro`
--
ALTER TABLE `libro`
  ADD PRIMARY KEY (`idlibro`,`Proveedor_idProveedor`,`categoria_idcategoria`),
  ADD KEY `fk_libro_Proveedor_idx` (`Proveedor_idProveedor`),
  ADD KEY `fk_libro_categoria1_idx` (`categoria_idcategoria`);

--
-- Indices de la tabla `proveedor`
--
ALTER TABLE `proveedor`
  ADD PRIMARY KEY (`idProveedor`);

--
-- Indices de la tabla `repartidor`
--
ALTER TABLE `repartidor`
  ADD PRIMARY KEY (`idrepartidor`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `administrador`
--
ALTER TABLE `administrador`
  MODIFY `idadministrador` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `carrito`
--
ALTER TABLE `carrito`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT de la tabla `categoria`
--
ALTER TABLE `categoria`
  MODIFY `idcategoria` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `cliente`
--
ALTER TABLE `cliente`
  MODIFY `idcliente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT de la tabla `compras`
--
ALTER TABLE `compras`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT de la tabla `domicilio`
--
ALTER TABLE `domicilio`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `libro`
--
ALTER TABLE `libro`
  MODIFY `idlibro` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT de la tabla `proveedor`
--
ALTER TABLE `proveedor`
  MODIFY `idProveedor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=105;

--
-- AUTO_INCREMENT de la tabla `repartidor`
--
ALTER TABLE `repartidor`
  MODIFY `idrepartidor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `libro`
--
ALTER TABLE `libro`
  ADD CONSTRAINT `fk_libro_Proveedor` FOREIGN KEY (`Proveedor_idProveedor`) REFERENCES `proveedor` (`idProveedor`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_libro_categoria1` FOREIGN KEY (`categoria_idcategoria`) REFERENCES `categoria` (`idcategoria`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
