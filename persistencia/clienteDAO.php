<?php
class ClienteDAO{
    private $id;
    private $nombre;
    private $apellido;
    private $correo;
    private $clave;
    private $fecha_nacimento;
    private $solicitud;
    
    public function __construct($id="",$nombre="", $apellido="", $correo="", $clave="", $fecha_nacimento="",$solicitud=""){
        $this -> id = $id;
        $this -> nombre = $nombre;
        $this -> apellido = $apellido;
        $this -> correo = $correo;
        $this -> clave = $clave;
        $this -> fecha_nacimento = $fecha_nacimento;
        $this -> solicitud = $solicitud;
    }
    
    public function crear(){
        return "insert into cliente(nombre, apellido, correo, clave, fecha_nacimiento)
            values('". $this -> nombre . "', '" . $this -> apellido . "', '" . $this -> correo . "', md5('" . $this -> clave . "'), '" . $this -> fecha_nacimento . "')";
    }

    public function actualizar()
    {
       return "update cliente set solicitud = '1'
        where idcliente = '".$this->id."'";
    }

    public function denegar()
    {
        return "update cliente set solicitud = '2'
        where idcliente = '" . $this->id . "'";
    }

    public function aceptar()
    {
        return "insert into proveedor (nombre, apellido,correo,clave)
          select nombre, apellido,correo,clave from cliente where idcliente = '".$this ->id."'";
    }

    public function eliminar()
    {
        return "delete from cliente where idcliente = '".$this->id."'";
    }
    
    public function consultarTodos(){
        return "select *
                from cliente";      
    }

    public function consultarid()
    {
        return "select idcliente
                from cliente";
    }
    public function consultarSolicitud()
    {
        return "select solicitud
                from cliente where idcliente = '".$this -> id."'";
    }

    public function autenticar()
    {
        return "select idcliente
                from cliente
                where correo = '" . $this->correo . "' and clave = md5('" . $this->clave . "')";
    }
    public function consultar()
    {
        return "select nombre, apellido, correo
                from cliente
                where idcliente = '" . $this->id . "'";
    }

    public function Solicitudes()
    {
        return "select idcliente,nombre, apellido, correo, fecha_nacimiento
                from cliente where solicitud = '1'";
    }
}
?>
