<?php
require_once 'persistencia/Conexion.php';
require_once 'persistencia/ClienteDAO.php';
class Cliente{
    private $id;
    private $nombre;
    private $apellido;
    private $correo;
    private $clave;
    private $fecha_nacimiento;
    private $solicitud;
    private $conexion;   
    private $clienteDAO;

    public function getId()
    {
        return $this->id;
    }

    public function getNombre()
    {
        return $this->nombre;
    }

    public function getApellido()
    {
        return $this->apellido;
    }

    public function getCorreo()
    {
        return $this->correo;
    }

    public function getFecha_nacimento()
    {
        return $this->fecha_nacimiento;
    }
    

    public function __construct($id="", $nombre="", $apellido="", $correo="", $clave="", $fecha_nacimiento="",$solicitud=""){
        $this -> id = $id;
        $this -> nombre = $nombre;
        $this -> apellido = $apellido;
        $this -> correo = $correo;
        $this -> clave = $clave;
        $this -> fecha_nacimiento = $fecha_nacimiento;
        $this-> solicitud = $solicitud;
        $this -> conexion = new Conexion();
        $this -> clienteDAO = new ClienteDAO($this->id, $this -> nombre, $this -> apellido, $this -> correo, $this -> clave, $this -> fecha_nacimiento, $this->solicitud);
    }

    public function actualizar()
    {
        $this->conexion->abrir();
        $this->conexion->ejecutar($this->clienteDAO->actualizar());
        $this->conexion->cerrar();
    }


    public function aceptar()
    {
        $this->conexion->abrir();
        $this->conexion->ejecutar($this->clienteDAO->aceptar());
        $this->conexion->ejecutar($this->clienteDAO->eliminar());
        $this->conexion->cerrar();
        
    }

    public function denegar()
    {
        $this->conexion->abrir();
        $this->conexion->ejecutar($this->clienteDAO->denegar());
        $this->conexion->cerrar();
    }
    
    public function crear(){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> clienteDAO -> crear());
        $this -> conexion -> cerrar();
    }
    
    public function consultarTodos(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> clienteDAO -> consultarTodos());
        $clientes = array();
        while(($registro = $this -> conexion -> extraer()) != null){            
            $cliente = new cliente($registro[0], $registro[1], $registro[2], $registro[3], $registro[4], $registro[5], $registro[6]);
            array_push($clientes,$cliente);
        }
        $this -> conexion -> cerrar();
        return  $clientes;
    }

    public function autenticar()
    {
        $this->conexion->abrir();
        $this->conexion->ejecutar($this->clienteDAO->autenticar());
        $this->conexion->cerrar();
        if ($this->conexion->numFilas() == 1) {
            $this->id = $this->conexion->extraer()[0];
            return true;
        } else {
            return false;
        }
    }

    public function consultarSolicitud()
    {
        $this->conexion->abrir();
        $this->conexion->ejecutar($this->clienteDAO->consultarSolicitud());
        $dato = array();
        while (($registro = $this->conexion->extraer()) != null) {
            $Solicitud = new cliente("","","","","","",$registro[0]);
            array_push($dato, $Solicitud);
        }
        $this->conexion->cerrar();
        return $dato;
    }
    public function __toString()
    {
        return $this->id . '' . $this->nombre . ' ' . $this->apellido . '' .
    $this-> correo .'' . $this->clave . ' ' . $this->fecha_nacimiento . ' ' . $this->solicitud ;
    }

    public function consultarId(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> clienteDAO -> consultarid());
        while(($registro = $this -> conexion -> extraer()) != null){
            $idcliente = new cliente($registro[0]);
            $proid = $registro[0];
        }
        $this -> conexion ->cerrar();
        return $proid;


    }

    public function Solicitudes()
    {
        $this->conexion->abrir();
        $this->conexion->ejecutar($this->clienteDAO->Solicitudes());
        $Solicitudes = array();
        while (($registro = $this->conexion->extraer()) != null) {
            $posible = new Cliente($registro[0], $registro[1], $registro[2], $registro[3],"", $registro[4],"");
            array_push($Solicitudes, $posible);
        }
        $this->conexion->cerrar();
        return  $Solicitudes;
    }

    
    
    public function consultar()
    {
        $this->conexion->abrir();
        $this->conexion->ejecutar($this->clienteDAO->consultar());
        $this->conexion->cerrar();
        $datos = $this->conexion->extraer();
        $this -> nombre = $datos[0];
        $this -> apellido = $datos[1];
        $this -> correo = $datos[2];
    }
}
?>