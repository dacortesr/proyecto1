<?php
require_once 'persistencia/Conexion.php';
require_once 'persistencia/CompraDAO.php';
class Compra
{
    private $id;
    private $idcliente;
    private $nombre;
    private $categoria;
    private $estado;
    private $compraDAO;

    public function getId()
    {
        return $this->id;
    }

    public function getIdcliente()
    {
        return $this->idcliente;
    }

    public function getNombre()
    {
        return $this->nombre;
    }

    public function getPrecio()
    {
        return $this->precio;
    }
    public function getCategoria()
    {
        return $this->categoria;
    }

    public function getEstado()
    {
        return $this->estado;
    }


    public function __construct($id = "",$idcliente = "", $nombre = "", $categoria = "",$estado="")
    {
        $this->id = $id;
        $this->idcliente = $idcliente;
        $this->nombre = $nombre;
        $this->categoria = $categoria;
        $this->estado = $estado;
        $this->conexion = new conexion();
        $this->compraDAO = new CompraDAO($this->id, $this->idcliente, $this->nombre, $this->categoria, $this->estado);
    }

    public function crear(){
        $this->conexion->abrir();
        $this->conexion->ejecutar($this->compraDAO->crear());
        $this->conexion->cerrar();
    }

    public function consultar()
    {
        $this->conexion->abrir();
        $this->conexion->ejecutar($this->compraDAO->consultar());
        $S = array();
        while (($registro = $this->conexion->extraer()) != null) {
            $p = new Compra("", "", $registro[0],$registro[1],"");
            array_push($S, $p);
        }
        $this->conexion->cerrar();
        return  $S;
    }
    public function ver()
    {
        $this->conexion->abrir();
        $this->conexion->ejecutar($this->compraDAO->ver());
        $S = array();
        while (($registro = $this->conexion->extraer()) != null) {
            $p = new Compra("", $registro[0],"","","");
            array_push($S, $p);
        }
        $this->conexion->cerrar();
        return  $S;
    }
}
