<?php
require_once 'persistencia/Conexion.php';
require_once 'persistencia/ReporteDAO.php';
class Reporte
{
    private $idcliente;
    private $idprovedor;
    private $nombre;
    private $categoria;
    private $precio;
    private $reporteDAO;

    public function getIdcliente()
    {
        return $this->idcliente;
    }

    public function getIdprovedor()
    {
        return $this->idprovedor;
    }

    public function getNombre()
    {
        return $this->nombre;
    }

    public function getCategoria()
    {
        return $this->categoria;
    }

    public function getPrecio()
    {
        return $this->precio;
    }


    public function __construct($idcliente = "", $idprovedor = "", $nombre = "", $categoria = "", $precio = "")
    {
        $this->idcliente = $idcliente;
        $this->idprovedor = $idprovedor;
        $this->nombre = $nombre;
        $this->categoria = $categoria;
        $this->precio = $precio;
        $this->conexion = new conexion();
        $this->reporteDAO = new ReporteDAO($this->idcliente, $this->idprovedor,$this->nombre, $this->categoria, $this->precio);
    }

    public function crear()
    {
        $this->conexion->abrir();
        $this->conexion->ejecutar($this->reporteDAO->crear());
        $this->conexion->cerrar();
    }

    public function consultar()
    {
        $this->conexion->abrir();
        $this->conexion->ejecutar($this->reporteDAO->consultar());
        $cate = array();
        while (($registro = $this->conexion->extraer()) != null) {
            $cas = new Reporte($registro[0],$registro[1],$registro[2], $registro[3], $registro[4]);
            array_push($cate, $cas);
        }
        $this->conexion->cerrar();
        return  $cate;
    }
    public function consultarPro()
    {
        $this->conexion->abrir();
        $this->conexion->ejecutar($this->reporteDAO->consultarPro());
        $cate = array();
        while (($registro = $this->conexion->extraer()) != null) {
            $cas = new Reporte($registro[0],$registro[1], $registro[2], $registro[3], $registro[4]);
            array_push($cate, $cas);
        }
        $this->conexion->cerrar();
        return  $cate;
    }
}
