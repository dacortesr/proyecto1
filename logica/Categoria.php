<?php
require_once 'persistencia/Conexion.php';
require_once 'persistencia/CategoriaDAO.php';
class Categoria{
    private $id;
    private $tipo;
    private $conexion;
    private $categoriaDAO;
    
    public function getId()
    {
        return $this->id;
    }

    public function getTipo()
    {
        return $this->tipo;
    }


    public function __construct($id="", $tipo=""){
        $this -> id = $id;
        $this -> tipo = $tipo;
        $this -> conexion = new conexion();
        $this -> categoriaDAO = new CategoriaDAO($this -> id, $this -> tipo);
    }
    public function consultarTodos(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> categoriaDAO -> consultarTodos());
        $cate = array();
        while(($registro = $this -> conexion -> extraer()) != null){
            $cas = new Categoria($registro[0],$registro[1]);
            array_push($cate, $cas);
        }
        $this -> conexion -> cerrar();
        return  $cate;
    }

    public function consultar()
    {
        $this->conexion->abrir();
        $this->conexion->ejecutar($this->categoriaDAO->consultar());
        $cate = array();
        while (($registro = $this->conexion->extraer()) != null) {
            $cas = new Categoria("", $registro[0]);
            array_push($cate, $cas);
        }
        $this->conexion->cerrar();
        return  $cate;
    }
    public function consultarRe()
    {
        $this->conexion->abrir();
        $this->conexion->ejecutar($this->categoriaDAO->consultarRe());
        $cate = array();
        while (($registro = $this->conexion->extraer()) != null) {
            $cas = new Categoria("", $registro[0]);
            array_push($cate, $cas);
        }
        $this->conexion->cerrar();
        return  $cate;
    }

    public function consultarId()
    {
        $this->conexion->abrir();
        $this->conexion->ejecutar($this->categoriaDAO->consultarId());
        $cate = array();
        while (($registro = $this->conexion->extraer()) != null) {
            $cas = new Categoria($registro[0]);
            array_push($cate, $cas);
        }
        $this->conexion->cerrar();
        return  $cate;
    }
    
}
