<?php
require_once 'persistencia/Conexion.php';
require_once 'persistencia/CarroDAO.php';
class Carro
{
    private $id;
    private $idcliente;
    private $idprovedor;
    private $nombre;
    private $categoria;
    private $precio;
    private $carroDAO;

    public function getId()
    {
        return $this->id;
    }

    public function getIdcliente()
    {
        return $this->idcliente;
    }

    public function getIdprovedor()
    {
        return $this->idprovedor;
    }

    public function getNombre()
    {
        return $this->nombre;
    }

    public function getCategoria()
    {
        return $this->categoria;
    }
    
    public function getPrecio()
    {
        return $this->precio;
    }


    public function __construct($id = "",$idcliente = "", $idprovedor = "", $nombre = "", $categoria = "",$precio="")
    {
        $this->id = $id;
        $this->idcliente = $idcliente;
        $this->idprovedor = $idprovedor;
        $this->nombre = $nombre;
        $this->categoria = $categoria;
        $this->precio = $precio;
        $this->conexion = new conexion();
        $this->carroDAO = new CarroDAO($this->id, $this->idcliente, $this->idprovedor, $this->nombre, $this->categoria, $this->precio);
    }

    public function crear(){
        $this->conexion->abrir();
        $this->conexion->ejecutar($this->carroDAO->crear());
        $this->conexion->cerrar();
    }

    public function eliminar2()
    {
        $this->conexion->abrir();
        $this->conexion->ejecutar($this->carroDAO->eliminar2());
        $this->conexion->cerrar();
    }

    public function eliminar()
    {
        $this->conexion->abrir();
        $this->conexion->ejecutar($this->carroDAO->eliminar());
        $this->conexion->cerrar();
    }

    public function Total()
    {
        $this->conexion->abrir();
        $this->conexion->ejecutar($this->carroDAO->Total());
        
        while (($registro = $this->conexion->extraer()) != null) {
            return $registro[0];
        }
       
        
    }
    public function consultar()
    {
        $this->conexion->abrir();
        $this->conexion->ejecutar($this->carroDAO->con());
        $cate = array();
        while (($registro = $this->conexion->extraer()) != null) {
            $cas = new Carro($registro[0],"",$registro[1],$registro[2],$registro[3], $registro[4]);
            array_push($cate, $cas);
        }
        $this->conexion->cerrar();
        return  $cate;
    }
}
