<?php
require_once 'persistencia/Conexion.php';
require_once 'persistencia/DomicilioDAO.php';
class Domicilio
{
    private $id;
    private $idcliente;
    private $direccion;
    private $estado;
    private $domicilioDAO;

    public function getId()
    {
        return $this->id;
    }

    public function getIdcliente()
    {
        return $this->idcliente;
    }

    public function getDireccion()
    {
        return $this->direccion;
    }

    public function getEstado()
    {
        return $this->estado;
    }


    public function __construct($id = "", $idcliente = "", $direccion = "", $estado = "")
    {
        $this->id = $id;
        $this->idcliente = $idcliente;
        $this->direccion = $direccion;
        $this->estado = $estado;
        $this->conexion = new conexion();
        $this->domicilioDAO = new DomicilioDAO($this->id, $this->idcliente, $this->direccion, $this->estado);
    }

    public function crear()
    {
        $this->conexion->abrir();
        $this->conexion->ejecutar($this->domicilioDAO->crear());
        $this->conexion->cerrar();
    }

    public function actualizar()
    {
        $this->conexion->abrir();
        $this->conexion->ejecutar($this->domicilioDAO->actualizar());
        $this->conexion->cerrar();
    }

    public function actualizar2()
    {
        $this->conexion->abrir();
        $this->conexion->ejecutar($this->domicilioDAO->actualizar2());
        $this->conexion->cerrar();
    }

    public function consultar()
    {
        $this->conexion->abrir();
        $this->conexion->ejecutar($this->domicilioDAO->consultar());
        $Solicitudes = array();
        while (($registro = $this->conexion->extraer()) != null) {
            $posible = new Domicilio($registro[0], $registro[1],"",$registro[2]);
            array_push($Solicitudes, $posible);
        }
        $this->conexion->cerrar();
        return  $Solicitudes;
    }

    public function repartidor()
    {
        $this->conexion->abrir();
        $this->conexion->ejecutar($this->domicilioDAO->repartidor());
        $Solicitudes = array();
        while (($registro = $this->conexion->extraer()) != null) {
            $posible = new Domicilio($registro[0],$registro[1], $registro[2]);
            array_push($Solicitudes, $posible);
        }
        $this->conexion->cerrar();
        return  $Solicitudes;
    }
    public function consultar2()
    {
        $this->conexion->abrir();
        $this->conexion->ejecutar($this->domicilioDAO->consultar2());
        $Solicitudes = array();
        while (($registro = $this->conexion->extraer()) != null) {
            $posible = new Domicilio($registro[0], $registro[1], "", $registro[2]);
            array_push($Solicitudes, $posible);
        }
        $this->conexion->cerrar();
        return  $Solicitudes;
    }

    public function consultar3()
    {
        $this->conexion->abrir();
        $this->conexion->ejecutar($this->domicilioDAO->consultar3());
        $Solicitudes = array();
        while (($registro = $this->conexion->extraer()) != null) {
            $posible = new Domicilio($registro[0], $registro[1],$registro[2], $registro[3]);
            array_push($Solicitudes, $posible);
        }
        $this->conexion->cerrar();
        return  $Solicitudes;
    }
    public function consultar4()
    {
        $this->conexion->abrir();
        $this->conexion->ejecutar($this->domicilioDAO->consultar4());
        $Solicitudes = array();
        while (($registro = $this->conexion->extraer()) != null) {
            $posible = new Domicilio($registro[0], $registro[1], $registro[2], $registro[3]);
            array_push($Solicitudes, $posible);
        }
        $this->conexion->cerrar();
        return  $Solicitudes;
    }
}
