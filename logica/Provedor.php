<?php
require_once 'persistencia/Conexion.php';
require_once 'persistencia/ProvedorDAO.php';
class Provedor{
    private $id;
    private $nombre;
    private $apellido;
    private $correo;
    private $clave;
    private $editorial;
    private $conexion;   
    private $provedorDAO;

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @return string
     */
    public function getApellido()
    {
        return $this->apellido;
    }

    /**
     * @return string
     */
    public function getCorreo()
    {
        return $this->correo;
    }

    /**
     * @return string
     */
    public function getEditorial()
    {
        return $this->editorial;
    }

    

    public function __construct($id = "",$nombre="", $apellido="", $correo="", $clave = "",$editorial = ""){
        $this -> id = $id;
        $this -> nombre = $nombre;
        $this -> apellido = $apellido;
        $this -> correo = $correo;
        $this -> clave = $clave;
        $this -> editorial = $editorial;
        $this -> conexion = new Conexion();
        $this -> provedorDAO = new ProvedorDAO($this -> id, $this -> nombre, $this -> apellido, $this -> correo, $this -> clave, $this -> editorial);
    }
    
    public function crear(){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> provedorDAO -> crear());
        $this -> conexion -> cerrar();
    }

    public function __toString()
    {
        return $this->id . '' . $this->nombre . ' ' . $this->apellido . '' .
        $this->correo . '' . $this->clave . ' ' . $this->editorial;
    }

    public function consultarId(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> provedorDAO -> consultarId());
        while (($registro = $this->conexion->extraer()) != null) {
            $proid = $registro[0];
        }
        $this->conexion->cerrar();
        return  $proid;
    }

    public function consultarTodos()
    {
        $this->conexion->abrir();
        $this->conexion->ejecutar($this->provedorDAO->consultarTodos());
        $provedores = array();
        while (($registro = $this->conexion->extraer()) != null) {
            $provedor = new Provedor($registro[0], $registro[1], $registro[2], $registro[3],"",$registro[4]);
            array_push($provedores, $provedor);
        }
        $this->conexion->cerrar();
        return  $provedores;
    }

    public function consultarTodos2()
    {
        $this->conexion->abrir();
        $this->conexion->ejecutar($this->provedorDAO->consultarTodos2());
        $provedores = array();
        while (($registro = $this->conexion->extraer()) != null) {
            $provedor = new Provedor($registro[0], $registro[1], $registro[2], $registro[3], $registro[4], $registro[5]);
            array_push($provedores, $provedor);
        }
        $this->conexion->cerrar();
        return  $provedores;
    }

    public function consultarTodo()
    {
        $this->conexion->abrir();
        $this->conexion->ejecutar($this->provedorDAO->consultarTodo());
        $provedores = array();
        while (($registro = $this->conexion->extraer()) != null) {
            $provedor = new Provedor($registro[0], $registro[1], $registro[2], $registro[3], "", $registro[4]);
            array_push($provedores, $provedor);
        }
        $this->conexion->cerrar();
        return  $provedores;
    }

    public function consultaru()
    {
        $this->conexion->abrir();
        $this->conexion->ejecutar($this->provedorDAO->consultaru());
        $provedores = array();
        while (($registro = $this->conexion->extraer()) != null) {
            $provedor = new Provedor($registro[0]);
            array_push($provedores, $provedor);
        }
        $this->conexion->cerrar();
        return  $provedores;
        $this->conexion->cerrar();
        
    }

    public function autenticar()
    {
        $this->conexion->abrir();
        $this->conexion->ejecutar($this->provedorDAO->autenticar());
        $this->conexion->cerrar();
        if ($this->conexion->numFilas() == 1) {
            $this->id = $this->conexion->extraer()[0];
            return true;
        } else {
            return false;
        }
    }

    

    public function consultar()
    {
        $this->conexion->abrir();
        $this->conexion->ejecutar($this->provedorDAO->consultar());
        $this->conexion->cerrar();
        $datos = $this->conexion->extraer();
        $this->nombre = $datos[0];
        $this->apellido = $datos[1];
        $this->correo = $datos[2];
    }
}
