<?php
require_once 'persistencia/Conexion.php';
require_once 'persistencia/RepartidorDAO.php';
class Repartidor{
    private $id;
    private $nombre;
    private $apellido;
    private $correo;
    private $clave;
    private $conexion;   
    private $repartidorDAO;

    public function getId()
    {
        return $this->id;
    }

    public function getNombre()
    {
        return $this->nombre;
    }

    public function getApellido()
    {
        return $this->apellido;
    }

    public function getCorreo()
    {
        return $this->correo;
    }
    

    public function __construct($id="", $nombre="", $apellido="", $correo="", $clave=""){
        $this -> id = $id;
        $this -> nombre = $nombre;
        $this -> apellido = $apellido;
        $this -> correo = $correo;
        $this -> clave = $clave;
        $this -> conexion = new Conexion();
        $this -> repartidorDAO = new RepartidorDAO($this->id, $this -> nombre, $this -> apellido, $this -> correo, $this -> clave);
    }

    public function consultar()
    {
        $this->conexion->abrir();
        $this->conexion->ejecutar($this->repartidorDAO->consultar());
        $this->conexion->cerrar();
        $datos = $this->conexion->extraer();
        $this->nombre = $datos[0];
        $this->apellido = $datos[1];
        $this->correo = $datos[2];
    }

    public function autenticar()
    {
        $this->conexion->abrir();
        $this->conexion->ejecutar($this->repartidorDAO->autenticar());
        $this->conexion->cerrar();
        if ($this->conexion->numFilas() == 1) {
            $this->id = $this->conexion->extraer()[0];
            return true;
        } else {
            return false;
        }
    }

    public function consultarTodos()
    {
        $this->conexion->abrir();
        $this->conexion->ejecutar($this->repartidorDAO->consultarTodos());
        $provedores = array();
        while (($registro = $this->conexion->extraer()) != null) {
            $provedor = new Provedor($registro[0], $registro[1], $registro[2], $registro[3]);
            array_push($provedores, $provedor);
        }
        $this->conexion->cerrar();
        return  $provedores;
    }
    public function __toString()
    {
        return $this->id . '' . $this->nombre . ' ' . $this->apellido . '' .
    $this-> correo .'' . $this->clave . ' ' . $this->fecha_nacimiento . ' ' . $this->solicitud ;
    }

}
