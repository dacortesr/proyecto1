<?php
require_once 'persistencia/Conexion.php';
require_once 'persistencia/LibroDAO.php';
class Libro{
    private $id;
    private $nombre;
    private $autor;
    private $precio;
    private $idprovedor;
    private $idcategoria;
    private $ruta;
    private $conexion;
    private $libroDAO;

    public function getId()
    {
        return $this->id;
    }

    public function getIdprovedor()
    {
        return $this->idprovedor;
    }
    public function getNombre()
    {
        return $this->nombre;
    }


    public function getPrecio()
    {
        return $this->precio;
    }

    public function getIdcategoria()
    {
        return $this->idcategoria;
    }

    public function getAutor()
    {
        return $this->autor;
    }

    public function getRuta()
    {
        return $this->ruta;
    }

    

    public function __construct($id="", $nombre="", $autor="", $precio="", $idprovedor="", $idcategoria ="", $ruta= ""){
        $this -> id = $id;
        $this -> nombre = $nombre;
        $this -> autor = $autor;
        $this -> precio = $precio;
        $this -> idprovedor = $idprovedor;
        $this -> idcategoria = $idcategoria;
        $this -> ruta = $ruta;
        $this -> conexion = new conexion();
        $this -> libroDAO = new LibroDAO($this -> id, $this -> nombre, $this -> autor, $this -> precio, $this ->idprovedor, $this->idcategoria, $this->ruta);
    }

    public function crear()
    {
        $this->conexion->abrir();
        $this->conexion->ejecutar($this->libroDAO->crear());
        $this->conexion->cerrar();
    }

    public function ver()
    {
        $this->conexion->abrir();
        $this->conexion->ejecutar($this->libroDAO->ver());
        $Solicitudes = array();
        while (($registro = $this->conexion->extraer()) != null) {
            $posible = new Libro("", $registro[0],"", $registro[1], "",$registro[2], $registro[3]);
            array_push($Solicitudes, $posible);
        }
        $this->conexion->cerrar();
        return  $Solicitudes;
    }

    public function conid()
    {
        $this->conexion->abrir();
        $this->conexion->ejecutar($this->libroDAO->conid());
        $Solicitudes = array();
        while (($registro = $this->conexion->extraer()) != null) {
            $posible = new Libro("", "", "", "", "", $registro[0], "");
            array_push($Solicitudes, $posible);
        }
        $this->conexion->cerrar();
        return  $Solicitudes;
    }

    public function id()
    {
        $this->conexion->abrir();
        $this->conexion->ejecutar($this->libroDAO->id());
        $Solicitudes = array();
        while (($registro = $this->conexion->extraer()) != null) {
            $posible = new Libro($registro[0]);
            array_push($Solicitudes, $posible);
        }
        $this->conexion->cerrar();
        return  $Solicitudes;
    }

    public function idlibro()
    {
        $this->conexion->abrir();
        $this->conexion->ejecutar($this->libroDAO->idlibro());
        $Solicitudes = array();
        while (($registro = $this->conexion->extraer()) != null) {
            $posible = new Libro("","","","",$registro[0]);
            array_push($Solicitudes, $posible);
        }
        $this->conexion->cerrar();
        return  $Solicitudes;
    }

    public function pro()
    {
        $this->conexion->abrir();
        $this->conexion->ejecutar($this->libroDAO->pro());
        $S = array();
        while (($registro = $this->conexion->extraer()) != null) {
            $p = new Libro($registro[0],$registro[1],$registro[2], $registro[3],$registro[4],$registro[5], $registro[6]);
            array_push($S, $p);
        }
        $this->conexion->cerrar();
        return  $S;
    }

    public function eliminar()
    {
        $this->conexion->abrir();
        $this->conexion->ejecutar($this->libroDAO->eliminar());
        $this->conexion->cerrar();
    }

    public function selec()
    {
        $this->conexion->abrir();
        $this->conexion->ejecutar($this->libroDAO->selec());
        $S = array();
        while (($registro = $this->conexion->extraer()) != null) {
            $p = new Libro("", $registro[0], "", $registro[1], "", "", $registro[2]);
            array_push($S, $p);
        }
        $this->conexion->cerrar();
        return  $S;
    }

    public function consultarReporte()
    {
        $this->conexion->abrir();
        $this->conexion->ejecutar($this->libroDAO->consultarReporte());
        $S = array();
        while (($registro = $this->conexion->extraer()) != null) {
            $p = new Libro("","","",$registro[0], "","","");
            array_push($S, $p);
        }
        $this->conexion->cerrar();
        return  $S;
    }

    public function __toString()
    {
        return $this->id . '' . $this->nombre . ' ' . $this->autor . '' .
        $this->precio . '' . $this->idprovedor . ' ' . $this->idcategoria . ' ' . $this->ruta;
    }

    public function actualizar()
    {
        $this->conexion->abrir();
        $this->conexion->ejecutar($this->libroDAO->actualizar());
        $this->conexion->cerrar();
    }

    public function actualiza()
    {
        $this->conexion->abrir();
        $this->conexion->ejecutar($this->libroDAO->actualiza());
        $this->conexion->cerrar();
    }
}
