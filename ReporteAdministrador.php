<?php
session_start();
require_once 'logica/Cliente.php';
require_once 'logica/Provedor.php';
require_once 'logica/Repartidor.php';
require_once 'logica/Categoria.php';
require_once 'logica/Libro.php';
require_once 'logica/Carro.php';
require_once 'logica/Compra.php';
require_once 'logica/Reporte.php';
require_once 'fpdf/fpdf.php';

$Clientes = new Cliente();
$Cli= $Clientes -> consultarTodos();

$Provedores = new Provedor();
$pro = $Provedores->consultarTodos2();

$Repartidor = new Repartidor();
$Repa = $Repartidor->consultarTodos();

$pdf = new FPDF('P','mm', 'Letter');
$pdf -> SetMargins(35, 10, 10);
$pdf -> AddPage();
$pdf -> Image("imagenes/hi.png", 10, 10, 20, 20);
$pdf -> SetFont('Times', 'B', 18);
//$pdf -> Text(20, 20, 'Hola Mundo');
$pdf -> Cell(170, 20, "Reportes Clientes, Provedores, Repartidores", 0, 1, 'C');

$pdf -> SetFont('Times', 'B', 10);

$pdf -> Cell(10, 8, "", 0, 0, 'C');
$pdf -> Cell(150, 8, "Clientes", 1, 1, 'C');

$pdf -> Cell(10, 8, "#", 1, 0, 'C');
$pdf -> Cell(50, 8, "Nombre ", 1, 0, 'C');
$pdf -> Cell(30, 8, "Apellido", 1, 0, 'C');
$pdf -> Cell(35, 8, "Correo", 1, 0, 'C');
$pdf->Cell(35, 8, "fecha nacimiento", 1, 1, 'C');

$pdf -> SetFont('Times', '', 10);
$i = 1;
foreach($Cli as $r){
$pdf->Cell(10, 8, $i++, 1, 0, 'C');
$pdf->Cell(50, 8, $r->getNombre(), 1, 0, 'C');
$pdf->Cell(30, 8, $r->getApellido(), 1, 0, 'C');
$pdf->Cell(35, 8, $r->getCorreo(), 1, 0, 'C');
$pdf->Cell(35, 8, $r->getFecha_nacimento(), 1, 1, 'C');
}
$pdf->Ln();
$pdf->SetFont('Times', 'B', 10);

$pdf->Cell(10, 8, "", 0, 0, 'C');
$pdf->Cell(150, 8, "Provedores", 1, 1, 'C');

$pdf->Cell(10, 8, "#", 1, 0, 'C');
$pdf->Cell(50, 8, "Nombre ", 1, 0, 'C');
$pdf->Cell(30, 8, "Apellido", 1, 0, 'C');
$pdf->Cell(35, 8, "Correo", 1, 0, 'C');
$pdf->Cell(35, 8, "editorial", 1, 1, 'C');

$pdf->SetFont('Times', '', 10);
$i = 1;
foreach ($pro as $p) {
    $pdf->Cell(10, 8, $i++, 1, 0, 'C');
    $pdf->Cell(50, 8, $p->getNombre(), 1, 0, 'C');
    $pdf->Cell(30, 8, $p->getApellido(), 1, 0, 'C');
    $pdf->Cell(35, 8, $p->getCorreo(), 1, 0, 'C');
    $pdf->Cell(35, 8, $p->getEditorial(), 1, 1, 'C');
}
$pdf->Ln();
$pdf->SetFont('Times', 'B', 10);

$pdf->Cell(10, 8, "", 0, 0, 'C');
$pdf->Cell(115, 8, "Repartidores", 1, 1, 'C');

$pdf->Cell(10, 8, "#", 1, 0, 'C');
$pdf->Cell(50, 8, "Nombre ", 1, 0, 'C');
$pdf->Cell(30, 8, "Apellido", 1, 0, 'C');
$pdf->Cell(35, 8, "Correo", 1, 1, 'C');

$pdf->SetFont('Times', '', 10);
$i = 1;
foreach ($Repa as $p) {
    $pdf->Cell(10, 8, $i++, 1, 0, 'C');
    $pdf->Cell(50, 8, $p->getNombre(), 1, 0, 'C');
    $pdf->Cell(30, 8, $p->getApellido(), 1, 0, 'C');
    $pdf->Cell(35, 8, $p->getCorreo(), 1, 0, 'C');
}

$pdf -> Output('I');
