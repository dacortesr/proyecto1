<?php
include 'presentacion/menuCliente.php';
$Cliente = new Cliente($_SESSION["id"]);
$Cliente->consultar();
?>
<div class="container">
    <?php include 'presentacion/carrito.php'; ?>
</div>

<div class="container">
    <div class="row bg-dark">
        <?php include 'presentacion/slider.php'; ?>
    </div>
</div>
<div class="container">
    <?php include 'presentacion/productos.php'; ?>
</div>