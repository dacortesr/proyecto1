<div class="container mt-3">
  <div class="row bg-primary">
    <div class="col-3 text-center">
      <h1>Mind Books<img src="imagenes/hi.png"></h1>
    </div>
    <div class="col-6 mt-3">
      <nav class="navbar navbar-light bg-primary">
        <div class="container-fluid">
          <form class="d-flex">
            <input class="form-control me-2" type="search" placeholder="Buscar" aria-label="Search">
            <button style="background-color: green;" type="submit">Buscar</button>
          </form>
        </div>
      </nav>
    </div>
    <div class="col-3 mt-3">
      <a class="link-light" href="index.php?pid=<?php echo base64_encode("presentacion/crear_cuenta.php") ?>" style="text-decoration:none">Crea tu cuenta</a>&nbsp&nbsp
      <a class="link-light" href="index.php?pid=<?php echo base64_encode("presentacion/ingresar.php") ?>" style="text-decoration:none">Ingresar</a>&nbsp&nbsp
    </div>
  </div>
  <div class=" row">
    <div class="col-12 bg-dark">
      <?php include 'presentacion/slider.php'; ?>
    </div>
  </div>
</div>