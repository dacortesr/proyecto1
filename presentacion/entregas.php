<?php
include 'presentacion/menuRepartidor.php';
require_once 'logica/Domicilio.php';
$Domicilio = new Domicilio();
foreach($Domicilio->repartidor() as $d){
    $Dom = new Domicilio($d->getId());
    if (isset($_POST["aceptar"])) {
        $Dom ->actualizar2();
    }
}



?>
<div class="container">
    <div class="row mt-3">
        <div class="col">
            <div class="card">
                <h5 class="card-header">Entregas pendientes</h5>
                <div class="card-body">
                    <?php if (isset($_POST["aceptar"])) { ?>
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            Solicitud Aceptada
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                        </div>
                    <?php } ?>
                    <table class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th scope="col">Nombre</th>
                                <th scope="col">Direccion</th>
                            </tr>
                        </thead>
                        <tbody>
                            
                                <?php
                                
                                    foreach ($Domicilio->repartidor() as $e) {
                                        echo "
                                    <form method='post' action='index.php?pid=" . base64_encode("presentacion/entregas.php") . "'>
                                    <tr>
                                        <td>" . $e->getIdcliente() . "</td>
                                        <td>" . $e->getDireccion() . "</td>
                                        <td><button type='submit' name='aceptar' class='btn btn-success'>Entregado</button></td>
                                        </tr>
                                        </form>";
                                    }

                                
                                
                                ?>
                            
                        </tbody>

                    </table>

                </div>
            </div>
        </div>
    </div>
</div>