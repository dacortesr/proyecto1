<?php
include "presentacion/menuProvedor.php";
require_once "logica/Categoria.php";
require_once "logica/Provedor.php";
require_once "logica/Libro.php";
$categoria = new Categoria();
$ca = $categoria->consultarTodos();
$provedor = new Provedor($_SESSION["id"]);
$pro = $provedor->consultarTodos();
if (isset($_REQUEST["agregar"])) {
    $nombre_imagen = $_FILES['foto']['name'];
    $temporal = $_FILES['foto']['tmp_name'];
    $carpeta = 'imagenes';
    $ruta = $carpeta .'/'.$nombre_imagen;
    move_uploaded_file($temporal, $carpeta . '/' . $nombre_imagen);
    foreach($ca as $idc){
        foreach ($pro as $p) {
            $libro = new Libro("", $_POST["nombre"],$p -> getNombre(), $_POST["precio"],$provedor, $_POST["cat"], $ruta);
            $lil = $libro->conid();
        }
        
    } 
    $li = $libro->crear();
}

?>
<div class="row bg-primary">
    <div class="col-6">

    </div>
</div>
<div class="container">
    <div class="row mt-3">
        <div class="col-xs-12 col-lg-4 text-center"></div>
        <div class="col-xs-12 col-lg-4 ">
            <div class="card">
                <h5 class="card-header bg-primary text-white">Agregar Producto</h5>
                <div class="card-body">
                    <?php if (isset($_POST["agregar"])) { ?>
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            Datos registrados correctamente
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                        </div>
                    <?php } ?>
                    <form method="post" action="index.php?pid=<?php echo base64_encode("presentacion/AccionProvedor/agregarProducto.php") ?>" enctype="multipart/form-data">
                        <div class="mb-3">
                            <input type="text" class="form-control" name="nombre" placeholder="Nombre" required="required" />
                        </div>
                        <div class="mb-3">
                            <input type="number" class="form-control" name="precio" placeholder="Precio" required="required">
                        </div>
                        <label for="exampleInputEmail1" class="form-label">Categoria</label>
                        <select class="form-select" name="cat">
                            <?php
                            foreach ($ca as $c) {
                                echo "<option value='" . $c->getId() . "'>" . $c->getTipo() . "</option>";
                            }
                            ?>
                        </select>
                        
                        <div class="file-field input-field mt-2">
                            <div>
                                <span>Elige una imagen</span>
                                <input class="text-center" type="file" name="foto" id="foto" onchange="vista_preliminar(event)" required="required">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-3"></div>
                        </div>
                        <div><img src="" alt="" id="img-foto"></div>
                        <div class="text-center mt-2">
                            <button type="submit" class="btn btn-primary" name="agregar">Agregar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    let vista_preliminar = (event) => {
        let leer_img = new FileReader();
        let id_img = document.getElementById('img-foto');

        leer_img.onload = () => {
            if (leer_img.readyState == 2) {
                id_img.src = leer_img.result;
            }
        }
        leer_img.readAsDataURL(event.target.files[0])
    }
</script>