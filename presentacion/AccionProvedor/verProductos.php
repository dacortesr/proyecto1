<?php
include 'presentacion/menuProvedor.php';
require_once "logica/Provedor.php";
require_once "logica/Libro.php";
require_once "logica/Categoria.php";

$cat = new Categoria();
$ca = $cat->consultarId();
$prov = new Provedor($_SESSION["id"]);

$Libro = new Libro("","","","",$prov,"","");
$li = $Libro->ver();



?>
<div class="container">
    <div class="row mt-3">
        <div class="col">
            <div class="card">
                <h5 class="card-header">Mis productos</h5>
                <div class="card-body">
                    <?php if (isset($_POST["eliminar"])) { ?>
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            Solicitud Aceptada
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                        </div>
                    <?php } ?>
                    <table class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th scope="col">Nombre</th>
                                <th scope="col">Precio</th>
                                <th scope="col">Categoria</th>
                                <th scope="col">Portada</th>
                            </tr>
                        </thead>
                        <tbody>
                            <form method="post" action="index.php?pid=<?php echo base64_encode("presentacion/AccionProvedor/verProductos.php") ?>">
                                <?php
                                foreach ($li as $lib) {
                                    
                                        echo "<tr>";
                                        echo "<td>" . $lib->getNombre() . "</td>";
                                        echo "<td>" . $lib->getPrecio() . "</td>";
                                    
                                        echo "<td>" . $lib->getIdcategoria() . "</td>";
                                    
                                        echo "<td> <img src=" . $lib->getRuta() . "> </td>";
                                    
                                }
                                ?>
                                
                                </tr>
                            </form>
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>