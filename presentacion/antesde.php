<?php
include 'presentacion/menuCliente.php';
require_once 'logica/Domicilio.php';
$Domicilio = new Domicilio("",$_SESSION["id"]);
$Dom = $Domicilio->consultar2();
$i = 1;
?>
<div class="container">
    <div class="row mt-3">
        <div class="col">
            <div class="card">
                <h5 class="card-header">Mis compras</h5>
                <div class="card-body">
                    <table class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th scope="col">Compra</th>
                            </tr>
                        </thead>
                        <tbody>
                            
                            <?php
                            
                            foreach ($Dom as $D) {
                                echo 
                                "<form method='post' action='index.php?pid=" . base64_encode('presentacion/domicilio.php'). "&id=".$D->getId()."'>
                                <tr>
                                <td> Paquete #  ". $i++. " </td>
                                <td> <button type = 'submit' class='btn btn-success' name='pedi'>Pedir </button></td>
                                </tr>
                                </form>";
                            }

                            ?>
                            
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>