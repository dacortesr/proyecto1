<?php
include "presentacion/menuAdministrador.php";
require_once "logica/Administrador.php";
$error = 0;
if (isset($_GET["error"])) {
    $error = $_GET["error"];
}
if (isset($_POST["actua"])) {
    $Administrado = new Administrador($_SESSION["id"], "", "", "", $_POST["clavep"]);
    $s = $Administrado->con();
    echo $s;
    if ($s == 1) {
        $Administrador = new Administrador($_SESSION["id"], "", "", "", $_POST["claven"]);
        $ad = $Administrador->cambiar();
    }
}

?>
<div class="container">
    <div class="row mt-3">
        <div class="col-xs-12 col-lg-4 text-center"></div>
        <div class="col-xs-12 col-lg-4 text-center">
            <div class="card">
                <h5 class="card-header bg-primary text-white">Editar clave</h5>
                <div class="card-body">
                    <?php if (isset($_POST["actua"])) { ?>
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            correcto
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                        </div>
                    <?php } ?>
                    <form method="post" action="index.php?pid=<?php echo base64_encode("presentacion/cambiarclave.php") ?>">
                        <div class="mb-3">
                            <input type="text" class="form-control" name="clavep" placeholder="Digite su clave actual">
                        </div>
                        <div class="mb-3">
                            <input type="text" class="form-control" name="claven" placeholder="Digite su nueva clave">
                        </div>
                        <button type="submit" class="btn btn-primary" name="actua">Actualizar</button>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>