<?php
include 'presentacion/menuProvedor.php';
require_once "logica/Reporte.php";
require_once "logica/Categoria.php";
$Reporte = new Reporte("", $_SESSION["id"]);
$as = $Reporte->consultarPro();
$datos = array();
$re = array();
foreach ($as as $r) {
$Categoria = new Categoria($r->getCategoria());
$cat = $Categoria->consultar();
foreach ($cat as $a) {
if (!array_key_exists($a->getTipo(), $re)) {
$re[$a->getTipo()] = 1;
} else {
$re[$a->getTipo()]++;
}
}
}


foreach ($as as $C) {
if (!array_key_exists($C->getNombre(), $datos)) {
$datos[$C->getNombre()] = 1;
} else {
$datos[$C->getNombre()]++;
}
}
?>
<div class="container">
    <div class="row mt-3">
        <div class="col">
            <div class="card">
                <h5 class="card-header">Estadisticas</h5>
                <div class="card-body">
                    <div id="piechart" style="height: 500px;"></div>
                    <div id="columnchart_values" style="height: 500px;"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    google.charts.load('current', {
        'packages': ['corechart']
    });
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {

        var data = google.visualization.arrayToDataTable([
            ['Aeropuerto', 'Cantidad'],
            <?php
            foreach ($datos as $key => $value) {
                echo "['" . $key . "', " . $value . "],";
            }
            ?>
        ]);

        var options = {
            title: 'Mis Ventas'
        };
        var chart = new google.visualization.PieChart(document.getElementById('piechart'));
        chart.draw(data, options);
    }
</script>
<script type="text/javascript">
    google.charts.load("current", {
        packages: ['corechart']
    });
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {
        var data = google.visualization.arrayToDataTable([
            ["Element", "Density", {
                role: "style"
            }],
            <?php
            foreach ($re as $key => $value) {
                echo "['" . $key . "'," . $value . ",'blue'],";
            }
            ?>

        ]);

        var view = new google.visualization.DataView(data);
        view.setColumns([0, 1,
            {
                calc: "stringify",
                sourceColumn: 1,
                type: "string",
                role: "annotation"
            },
            2
        ]);

        var options = {
            title: "Cantidad de libros respecto a su categoria",
            width: 600,
            height: 400,
            bar: {
                groupWidth: "95%"
            },
            legend: {
                position: "none"
            },
        };
        var chart = new google.visualization.ColumnChart(document.getElementById("columnchart_values"));
        chart.draw(view, options);
    }
</script>