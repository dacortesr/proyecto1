<?php
include 'presentacion/menuAdministrador.php';
$pro = new Provedor();
$provedor = $pro->consultarTodo();
?>
<div class="container">
    <div class="row mt-3">
        <div class="col">
            <div class="card">
                <h5 class="card-header">Consultar Provedores</h5>
                <div class="card-body">
                    <table class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th scope="col">Codigo</th>
                                <th scope="col">Nombre</th>
                                <th scope="col">Apellido</th>
                                <th scope="col">Correo</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            
                            foreach ($provedor as $provedoresa) {

                                echo "<tr>";
                                echo "<td>" . $provedoresa->getId() . "</td>";
                                echo "<td>" . $provedoresa->getNombre() . "</td>";
                                echo "<td>" . $provedoresa->getApellido() . "</td>";
                                echo "<td>" . $provedoresa->getCorreo() . "</td>";
                                echo "</tr>";
                            }
                           
                            ?>
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>