<?php
$clie = new Cliente();
foreach($clie->Solicitudes() as $cl){
    $r = new Cliente($cl ->getId());
    if (isset($_POST["denegar"])) {
        $r->denegar();
    }
    if (isset($_POST["aceptar"])) {
        $r->aceptar();
    }
}

include 'presentacion/menuAdministrador.php';
?>
<div class="container">
    <div class="row mt-3">
        <div class="col">
            <div class="card">
                <h5 class="card-header">Solicitudes pendientes</h5>
                <div class="card-body">
                    <?php if (isset($_POST["denegar"])) { ?>
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            Solicitud Denegada
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                        </div>
                    <?php } ?>
                    <?php if (isset($_POST["aceptar"])) { ?>
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            Solicitud Aceptada
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                        </div>
                    <?php } ?>
                    <table class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th scope="col">Codigo</th>
                                <th scope="col">Nombre</th>
                                <th scope="col">Apellido</th>
                                <th scope="col">Correo</th>
                                <th scope="col">Fecha nacimiento</th>
                            </tr>
                        </thead>
                        <tbody>
                            <form method="post" action="index.php?pid=<?php echo base64_encode("presentacion/Proveedores/solicitudes.php") ?>">
                                <?php

                                foreach ($clie->Solicitudes() as $provedoresa) {

                                    echo "<tr>";
                                    echo "<td>" . $provedoresa->getId() . "</td>";
                                    echo "<td>" . $provedoresa->getNombre() . "</td>";
                                    echo "<td>" . $provedoresa->getApellido() . "</td>";
                                    echo "<td>" . $provedoresa->getCorreo() . "</td>";
                                    echo "<td>" . $provedoresa->getFecha_nacimento() . "</td>";
                                    echo "<td><button type='submit' name='aceptar' class='btn btn-success'>Aceptar</button></td>";
                                    echo "<td><button type='submit' name='denegar' class='btn btn-danger'>Denegar</button></td>";
                                    echo "</tr>";
                                }

                                ?>
                            </form>
                        </tbody>

                    </table>

                </div>
            </div>
        </div>
    </div>
</div>