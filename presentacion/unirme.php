<?php
include 'presentacion/menuCliente.php';
$cliente = new Cliente($_SESSION["id"], "", "", "", "", "", "");
$esta = "";
if (isset($_POST["enviar"])) {
    $cliente->actualizar();
}
?>
<div class="container mt-2">
    <div class="row ">
        <div class="col-xs-12 col-lg-4 text-center"></div>
        <div class="col-xs-12 col-lg-4 text-center">
            <div class="card">
                <h5 class="card-header <?php
                foreach ($cliente->consultarSolicitud() as $da){
                    if ($da == "0") {
                        $esta = "bg-success";
                    } else if ($da == "2") {
                        $esta = "bg-danger";
                    } else  {
                        $esta = "bg-primary";
                    }
                }
                
                echo $esta ?>">Unirse</h5>
                <div class="card-body">
                    <?php if (isset($_POST["enviar"])) { ?>

                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            Tu solicitud ha sido enviada
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                        </div>
                    <?php } ?>
                    <?php
                    foreach ($cliente->consultarSolicitud() as $da)
                        if ($da == "0") {
                    ?>
                    
                        <form method="post" action="index.php?pid=<?php echo base64_encode("presentacion/unirme.php") ?>">
                            <h5 class="card-title">Solicitud para ser proveedor</h5>
                            <p class="card-text">Se enviara tu solicitud al Administrador,ten en cuenta que 
                                Debes tener una editorial para ser proveedor
                            </p>
                            <button type="submit" class="btn btn-success mt-2" name="enviar">Enviar</button>
                        </form>
                    <?php
                        } else if ($da == "2") {
                    ?>
                        <h5 class="card-title">Solicitud para ser proveedor</h5>
                        <p class="card-text">La solicitud fue Rechazada,Lo sentimos
                        </p>
                    <?php
                        } else {
                       
                    ?>
                        <h5 class="card-title">Solicitud para ser proveedor</h5>
                        <p class="card-text">La solicitud ya fue enviada,Esperando respuesta</p>


                    <?php
                        }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>