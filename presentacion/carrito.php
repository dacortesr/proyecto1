<?php
require_once "logica/Libro.php";
require_once "logica/Carro.php";
require_once "logica/Compra.php";
require_once "logica/Reporte.php";
require_once "logica/Domicilio.php";
$dato;
$Carrito = new Carro("", $_SESSION["id"]);
$Total = $Carrito->Total();

if (isset($_POST["pagar"])) {
    foreach ($Carrito->consultar() as $g) {
        $Compra = new Compra("", $_SESSION["id"], $g->getNombre(), $g->getCategoria());
        $Compra->crear();
        $Carrito->eliminar();
        $Reporte = new Reporte($_SESSION["id"], $g->getIdprovedor(), $g->getNombre(), $g->getCategoria(), $g->getPrecio());
        $Reporte->crear();
    }
    $Domicilio = new Domicilio("", $_SESSION["id"]);
    $Domicilio->crear();
}
if (isset($_POST["eliminar"])) {

    foreach ($Carrito->consultar() as $w) {
        $Car = new Carro($w->getId());
        $Car->eliminar2();
        $Total = 0;
    }
}
?>



<div class='row bg-primary'>
    <nav class='navbar navbar-expand-lg navbar-dark bg-dark'>
        <div class='container-fluid'>
            <button class='navbar-toggler' type='button' data-bs-toggle='collapse' data-bs-target='#navbarNavDropdown' aria-controls='navbarNavDropdown' aria-expanded='false' aria-label='Toggle navigation'>
                <span class='navbar-toggler-icon'></span>
            </button>
            <div class='collapse navbar-collapse' id='navbarNavDropdown'>
                <ul class='navbar-nav'>
                    <li class='nav-item'>
                        <a class='nav-link' data-bs-toggle='modal' data-bs-target='#modal_cart' style='color: red;'><i class='fas fa-shopping-cart'></i> </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <div class='modal fade' id='modal_cart' tabindex='-1' aria-hidden='true'>
        <div class='modal-dialog'>
            <div class='modal-content'>
                <div class='modal-header'>
                    <h5 class='modal-title' id='exampleModalLabel'>Carro de compras</h5>
                    <button type="button" class=" btn-close" data-bs-dismiss="modal" aria-label='Close'></button>
                </div>

                <div class='modal-body'>
                    <div class='modal-body'>
                        <div>
                            <form method="post" action="index.php?pid=<?php echo base64_encode("presentacion/sesionCliente.php") ?>">
                                <div class='p-2'>
                                    <table class="table table-striped table-hover">
                                        <thead>
                                            <tr>
                                                <th scope="col">Nombre</th>
                                                <th scope="col">Unidades</th>
                                                <th scope="col">Precio</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php

                                            foreach ($Carrito->consultar() as $c) {
                                                $precio = 0;
                                                echo "<tr>";
                                                echo "<td>" . $c->getNombre() . "</td>";
                                                echo "<td> <input type='number' id='numero' min='1' max='80' value='1' class='text-center'></td>";
                                                echo "<td>" . $c->getPrecio() . "</td>";
                                                echo "</tr>";
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                    <table class="table table-striped table-hover">
                                        <tbody>
                                            <?php
                                            echo "<tr>";
                                            echo "<td> <b>Total:   </b>" . $Total . "</td>";
                                            echo "</tr>";
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                        </div>
                    </div>


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>
                    <button type='submit' name='pagar' class='btn btn-success'>Pagar</button>
                    <button type='submit' name='eliminar' class='btn btn-danger'>Vaciar Carrito</button>";
                </div>
                </form>
            </div>
        </div>

        <script>
            $(function() {
                $("#numero").change(function() {
                    var recibir = $(this).val();
                    $("#eso").val(recibir);
                    $dato = recibir;
                });
            });
        </script>