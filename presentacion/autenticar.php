<?php
$correo = $_POST["correo"];
$clave = $_POST["clave"];

$administrador = new Administrador("", "", "", $correo, $clave);
$Cliente = new Cliente("","","", $correo, $clave, "");
$Provedor = new Provedor("", "", "", $correo, $clave, "");
$Repartidor = new Repartidor("", "", "", $correo, $clave);

if($administrador -> autenticar()){
    $_SESSION["id"] = $administrador -> getId();     
    $_SESSION["rol"] = "administrador";
    header("Location: index.php?pid=" . base64_encode("presentacion/sesionAdministrador.php"));
} else if ($Cliente->autenticar()) {
    $_SESSION["id"] = $Cliente->getId();
    $_SESSION["rol"] = "cliente";
    header("Location: index.php?pid=" . base64_encode("presentacion/sesionCliente.php"));
} else if ($Provedor->autenticar()) {
    $_SESSION["id"] = $Provedor->getId();
    $_SESSION["rol"] = "provedor";
    header("Location: index.php?pid=" . base64_encode("presentacion/sesionProvedor.php")); 
}else if ($Repartidor->autenticar()) {
    $_SESSION["id"] = $Repartidor->getId();
    $_SESSION["rol"] = "Repartidor";
    header("Location: index.php?pid=" . base64_encode("presentacion/sesionRepartidor.php"));
    
} else {
    header("Location: index.php?pid=" . base64_encode("presentacion/ingresar.php") . "&error=1");
}
?>




