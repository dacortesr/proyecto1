<?php
$Cliente = new Cliente($_SESSION["id"]);
$Cliente->consultar();
?>
<div class="container mt-3">
    <div class="row bg-primary">
        <div class="col-3 text-center">
            <h1>Mind Books<img src="imagenes/hi.png"></h1>
        </div>
        <div class="col-4 mt-3">
            <nav class="navbar navbar-light bg-primary">
                <div class="container-fluid">
                    <form class="d-flex">
                        <input class="form-control me-2" type="search" placeholder="Buscar" aria-label="Search">
                        <button style="background-color: green;" type="submit">Buscar</button>
                    </form>
                </div>
            </nav>
        </div>
        <div class="col-5 mt-3">
            <nav class="navbar navbar-expand-lg navbar-light bg-primary">
                <div class="container-fluid">
                    <a class="navbar-brand" href="index.php?pid=<?php echo base64_encode("presentacion/sesionCliente.php") ?>"><i class="fas fa-home"></i></a>
                    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarNav">
                        <ul class="navbar-nav">
                            <li class="nav-item dropdown"><a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Cliente: <?php echo $Cliente->getNombre() . " " . $Cliente->getApellido() ?></a>
                                <div class="dropdown-menu">
                                    <a class="dropdown-item" href="#">Editar Perfil</a>
                                    <a class="dropdown-item" href="#">Cambiar Clave</a>
                                    <a class="dropdown-item" href="reporte.php" target="blank">Reporte</a>
                                    <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/Proveedores/estadisticascliente.php") ?>" target="_blank">Estadisticas</a>
                                </div>
                            </li>
                            <li class="nav-item"><a class="nav-link" href="index.php?pid=<?php echo base64_encode("presentacion/antesde.php") ?>">Domicilio</a></li>
                            <li class="nav-item"><a class="nav-link" href="index.php?pid=<?php echo base64_encode("presentacion/unirme.php") ?>">Unirme</a></li>
                            <li class="nav-item"><a class="nav-link" href="index.php?sesion=false">Cerrar Sesion</a></li>

                        </ul>
                    </div>
                </div>
            </nav>
        </div>
    </div>
</div>