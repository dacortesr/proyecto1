<?php
$Provedor = new Provedor($_SESSION["id"]);
$Provedor->consultar();
?>
<div class="container mt-3">
    <div class="row bg-primary">
        <div class="col-3 text-center">
            <h1>Mind Books<img src="imagenes/hi.png"></h1>
        </div>
        <div class="col-3 mt-3">
            <nav class="navbar navbar-light bg-primary">
                <div class="container-fluid">
                    <form class="d-flex">
                        <input class="form-control me-2" type="search" placeholder="Buscar" aria-label="Search">
                        <button style="background-color: green;" type="submit">Buscar</button>
                    </form>
                </div>
            </nav>
        </div>
        <div class="col-6 mt-3">
            <nav class="navbar navbar-expand-lg navbar-light bg-primary">
                <div class="container-fluid">
                    <a class="navbar-brand" href="index.php?pid=<?php echo base64_encode("presentacion/sesionProvedor.php") ?>"><i class="fas fa-home"></i></a>
                    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarNav">
                        <ul class="navbar-nav">
                            <li class="nav-item dropdown"><a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Provedor: <?php echo $Provedor->getNombre() . " " . $Provedor->getApellido() ?></a>
                                <div class="dropdown-menu">
                                    <a class="dropdown-item" href="#">Editar Perfil</a> <a class="dropdown-item" href="#">Cambiar Clave</a>
                                </div>
                            </li>
                            <li class="nav-item dropdown"><a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Mis productos</a>
                                <div class="dropdown-menu">
                                    <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/AccionProvedor/agregarProducto.php") ?>">Añadir producto</a>
                                    <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/AccionProvedor/verProductos.php") ?>">Ver productos</a>
                                    <a class="dropdown-item" href="ReporteProvedor.php">Reporte</a>
                                    <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/Proveedores/estadisticasProvedor.php") ?> " target="_blank">Estadisticas</a>
                                </div>
                            </li>
                            <li class=" nav-item"><a class="nav-link" href="index.php?sesion=false">Cerrar Sesion</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        </div>
    </div>
</div>