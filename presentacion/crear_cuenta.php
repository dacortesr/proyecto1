<?php
require_once 'logica/cliente.php';
if (isset($_POST["crear"])) {
    $idl = new cliente();
    $id = $idl->consultarId();
    $clie = new cliente("", $_POST["nombre"], $_POST["apellido"], $_POST["correo"], $_POST["clave"], $_POST["fecha"],"");
    $clie->crear();
}
?>

<div class="container mt-2">
    <div class="row bg-primary">
        <div class="col-4 text-center">
            <a class="link-dark" style="text-decoration:none" href="index.php">
                <h1>Mind Books<img src="imagenes/hi.png"></h1>
            </a>
        </div>
        <div class="col-5">

        </div>
        <div class=" col-3 mt-3">
            <a class="link-light" href="index.php?pid=<?php echo base64_encode("presentacion/crear_cuenta.php") ?>" style="text-decoration:none">Crea tu cuenta</a>&nbsp&nbsp
            <a class="link-light" href="index.php?pid=<?php echo base64_encode("presentacion/ingresar.php") ?>" style="text-decoration:none">Ingresar</a>&nbsp&nbsp
        </div>
    </div>
    <div class="row mt-3">
        <div class="col-xs-12 col-lg-4 text-center"></div>
        <div class="col-xs-12 col-lg-4 ">
            <div class="card">
                <div class="text-center">
                    <h5 class="card-header bg-primary text-white">Crear cuenta</h5>
                </div>
                <div class="card-body">
                    <?php if (isset($_POST["crear"])) { ?>
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            Datos registrados correctamente
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                        </div>
                    <?php } ?>
                    <form method="post" action="index.php?pid=<?php echo base64_encode("presentacion/crear_cuenta.php") ?>">
                        <div>
                            <label for="exampleInputEmail1" class="form-label">Nombre</label>
                            <input type="text" class="form-control" name="nombre" required="required">
                        </div>
                        <div>
                            <label for="exampleInputEmail1" class="form-label">Apellido</label>
                            <input type="text" class="form-control" name="apellido" required="required">
                        </div>
                        <div>
                            <label for="exampleInputEmail1" class="form-label">Correo</label>
                            <input type="email" class="form-control" name="correo" required="required">
                        </div>
                        <div>
                            <label for="exampleInputEmail1" class="form-label">Clave</label>
                            <input type="password" class="form-control" name="clave" required="required">
                        </div>
                        <div class="mb-3">
                            <label for="exampleInputEmail1" class="form-label">Fecha de nacimiento</label>
                            <input type="date" class="form-control" name="fecha" required="required">
                        </div>
                        <div class="text-center">
                            <button type="submit" name="crear" class="btn btn-primary">Crear</button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>